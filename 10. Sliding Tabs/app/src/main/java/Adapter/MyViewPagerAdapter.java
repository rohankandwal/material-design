package Adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.itcse.rohankandwal.slidingtabs.SimpleFragment;

/**
 * Created by User on 30-04-2015.
 */
public class MyViewPagerAdapter extends FragmentPagerAdapter {

    String[] title = new String[]{"Tab 1", "Tab 2", "Tab 3", "Tab 4",};
    public MyViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        SimpleFragment simpleFragment = new SimpleFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        simpleFragment.setArguments(args);
        return simpleFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }

    @Override
    public int getCount() {
        return 3;
    }
}
