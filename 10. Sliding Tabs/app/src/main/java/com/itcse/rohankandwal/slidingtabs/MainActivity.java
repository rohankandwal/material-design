package com.itcse.rohankandwal.slidingtabs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import Adapter.MyViewPagerAdapter;
import slidingTab.SlidingTabLayout;


public class MainActivity extends ActionBarActivity {

    ViewPager mPager;
    SlidingTabLayout mSlidingTab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        NavigationDrawerFragment navigationDrawerFragment =
                (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);
        navigationDrawerFragment.setUp((DrawerLayout)findViewById(R.id.drawer_layout)
        , toolbar);

        mPager = (ViewPager) findViewById(R.id.pager);
        mSlidingTab = (SlidingTabLayout) findViewById(R.id.slidingTab);
        mSlidingTab.setDistributeEvenly(true);
        mPager.setAdapter(new MyViewPagerAdapter(getSupportFragmentManager()));

        mSlidingTab.setViewPager(mPager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*switch (item.getItemId()) {
            case R.id.call:
                Toast.makeText(MainActivity.this, "call", Toast.LENGTH_SHORT)
                        .show();
                Intent intent = new Intent(this, SubActivity.class);
                startActivity(intent);
                return true;

            case R.id.sms:
                Toast.makeText(MainActivity.this, "sms", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.statistic:
                Toast.makeText(MainActivity.this, "statistic", Toast.LENGTH_SHORT)
                        .show();
                return true;

            case R.id.about:

                Toast.makeText(MainActivity.this, "about", Toast.LENGTH_SHORT)
                        .show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }*/
        return super.onOptionsItemSelected(item);
    }
}
