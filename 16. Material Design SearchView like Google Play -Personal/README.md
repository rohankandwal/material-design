This project can emulate the functionalities like that in Google Play app. I have used AutoCompleteTextView to show suggested search results.
The project contains a SearchBar like Google Play app with a "Hamburger Icon", AutoCompleteTextView and a "Voice Search" icon. When user isn't
searching for anything, clicking on the Hamburger Icon will open the "DrawerMenu". If the user clicks on AutoCompleteTextView to enter any text,
then the Hamburger icon animates to "Arrow" icon. If the user clicks on "Arrow", the AutoCompleteTextView focus is lost and default text is shown,
while the entered text is saved to be populated when user re-focuses the AutoCompleteTextView.

Please see the screenshots to understand the app behaviour.

Since the only compatability library I have used is "Material Menu" so anyone can use this project, modify it according to their need and use it accordingly.

