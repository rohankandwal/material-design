package com.itcse.materialdesignsearchviewlikegoogleplay;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import com.balysv.materialmenu.MaterialMenuDrawable;

import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private MaterialMenuDrawable materialMenu;
    int iconState = 0; // 0= Hamburger, 1=arrow
    private boolean isVoiceRecognitionIntentSupported;
    private ImageView ivEndIcon;
    String enteredText = "";
    private AutoCompleteTextView searchAutoComplete;
    private Toolbar toolbar;
    private ImageView ivSpeakNow;
    final String defaultSearchText = "Search Items or Products";
    private InputMethodManager imm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        isVoiceRecognitionIntentSupported = isIntentAvailable(this, new Intent("android.speech.action.RECOGNIZE_SPEECH"));

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        final NavigationDrawerFragment navigationDrawerFragment =
                (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragment);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationDrawerFragment.setUp( mDrawerLayout
                , toolbar);

        mDrawerLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                drawerOpened();
                return false;
            }
        });
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle your drawable state here
                if (materialMenu.getIconState() == MaterialMenuDrawable.IconState.BURGER) {
                    navigationDrawerFragment.syncState();
                } else {
                    materialMenu.animateIconState(MaterialMenuDrawable.IconState.BURGER);
                    if (searchAutoComplete.getText().toString().length() != 0 && !enteredText.equals(defaultSearchText))
                        enteredText = searchAutoComplete.getText().toString();
                    searchAutoComplete.clearFocus();
                    imm.hideSoftInputFromWindow(searchAutoComplete.getWindowToken(), 0);
                    manageState();
                }
            }
        });

        materialMenu = new MaterialMenuDrawable(this, Color.WHITE, MaterialMenuDrawable.Stroke.THIN);
        materialMenu.setColor(Color.BLACK);
        toolbar.setNavigationIcon(materialMenu);

        searchAutoComplete = (AutoCompleteTextView) findViewById(R.id.atvSearch);
        searchAutoComplete.setText(defaultSearchText);
//        searchAutoComplete.setText("Search Items or Products");
        ivEndIcon = (ImageView) findViewById(R.id.ivEndIcon);
        ivSpeakNow = (ImageView) findViewById(R.id.ivSpeakNow);
        if (!isVoiceRecognitionIntentSupported) {
            ivSpeakNow.setVisibility(View.INVISIBLE);
        }

        ivEndIcon.setOnClickListener(this);
        ivSpeakNow.setOnClickListener(this);

        String[] countries = getResources().getStringArray(R.array.countries_array);

        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, countries);

        searchAutoComplete.setAdapter(adapter);

        searchAutoComplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    materialMenu.animateIconState(MaterialMenuDrawable.IconState.ARROW);
                    if (!enteredText.equals(defaultSearchText))
                        searchAutoComplete.setText(enteredText);
                    if (!enteredText.equals(""))
                        searchAutoComplete.setSelection(enteredText.length());
                    imm.showSoftInput(searchAutoComplete, InputMethodManager.SHOW_IMPLICIT);
//                    ivEndIcon.setVisibility(View.GONE);
                    manageState();
                } else {
                    drawerOpened();
                }
            }
        });

        searchAutoComplete.addTextChangedListener(searchTextWatcher);
    }

    TextWatcher searchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            manageState();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

/*
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof AutoCompleteTextView) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                } else
                    return false;
            }
        }
        return super.dispatchTouchEvent(event);
    }
*/

    private boolean isIntentAvailable(Context context, Intent intent) {
        PackageManager mgr = context.getPackageManager();
        List list = mgr.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.ivEndIcon:
                // If the toggle icon state is arrow then the icon set will be clear icon
                if (materialMenu.getIconState() == MaterialMenuDrawable.IconState.ARROW
                        && searchAutoComplete.getText().length() != 0) {
                    searchAutoComplete.setText("");
                    enteredText = "";
                }
                break;
            case R.id.ivSpeakNow:
                activateVoiceSearch();
                break;
        }
    }

    public void activateVoiceSearch() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-IN");

        try {
            startActivityForResult(intent, 1);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(MainActivity.this, "Oops! Your device doesn't support Speech to Text", Toast.LENGTH_SHORT).show();
        }
    }

    /* When Mic activity close */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1: {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    String yourResult = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS).get(0);
                    Toast.makeText(MainActivity.this, yourResult, Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    private void manageState() {
        // If we have opened the search autocomplete to enter text
        if (searchAutoComplete.hasFocus()) {
            if (searchAutoComplete.getText().length() == 0) {
                if (isVoiceRecognitionIntentSupported) {
                    ivSpeakNow.setVisibility(View.VISIBLE);
                    ivEndIcon.setVisibility(View.GONE);
                } else {
                    ivSpeakNow.setVisibility(View.GONE);
                }
            } else {
                ivEndIcon.setVisibility(View.VISIBLE);
                ivSpeakNow.setVisibility(View.GONE);
            }
        } else {
            if (isVoiceRecognitionIntentSupported) {
                ivSpeakNow.setVisibility(View.VISIBLE);
                ivEndIcon.setVisibility(View.GONE);
            } else {
                ivSpeakNow.setVisibility(View.INVISIBLE);
                ivEndIcon.setVisibility(View.GONE);
            }
        }
    }

    public void drawerOpened() {
        materialMenu.animateIconState(MaterialMenuDrawable.IconState.BURGER);
        if (!searchAutoComplete.getText().toString().equals(defaultSearchText)
                && searchAutoComplete.getText().length() > 0) {
            enteredText = searchAutoComplete.getText().toString();
        }
        searchAutoComplete.setText(defaultSearchText);
        manageState();
        if (getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        searchAutoComplete.clearFocus();

/*        if (imm.isActive())
        imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);*/
    }
}
