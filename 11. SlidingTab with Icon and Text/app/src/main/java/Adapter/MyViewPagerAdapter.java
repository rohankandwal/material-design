package Adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

import com.itcse.rohankandwal.slidingtabwithiconandtext.MainActivity;
import com.itcse.rohankandwal.slidingtabwithiconandtext.R;
import com.itcse.rohankandwal.slidingtabwithiconandtext.SimpleFragment;


/**
 * Created by User on 30-04-2015.
 */
public class MyViewPagerAdapter extends FragmentPagerAdapter {

    String[] title = new String[]{"Tab 1", "Tab 2", "Tab 3", "Tab 4",};
    Activity activity;
    public MyViewPagerAdapter(FragmentManager fm, MainActivity mainActivity) {
        super(fm);
        this.activity = mainActivity;
    }

    @Override
    public Fragment getItem(int position) {
        SimpleFragment simpleFragment = new SimpleFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        simpleFragment.setArguments(args);
        return simpleFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable drawable = activity.getResources().getDrawable(R.mipmap.ic_launcher);
        drawable.setBounds(0,0,48,48);
        ImageSpan imageSpan = new ImageSpan(drawable);

        SpannableString spannableString = new SpannableString(title[position] + " ");
        spannableString.setSpan(imageSpan, spannableString.length()-1, spannableString.length(), 0);
        return spannableString;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
