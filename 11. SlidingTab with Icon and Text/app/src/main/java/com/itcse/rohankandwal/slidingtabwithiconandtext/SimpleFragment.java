package com.itcse.rohankandwal.slidingtabwithiconandtext;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by User on 30-04-2015.
 */
public class SimpleFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.simple_fragment, container, false);
        Bundle bundle = getArguments();
        if (bundle!=null){
            TextView textView = (TextView) view.findViewById(R.id.textView);
            textView.setText("The position is " + bundle.get("position"));
        }
        return view;
    }
}
